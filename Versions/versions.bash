#!/bin/bash -euo pipefail

# Script generates a new file named versions.h
# versions.h contains three values for AFSEBundleShortVersionString and
# AFSEBundleVersion used for versioning.
# Versioning scheme now is as follows:
# environment.RC.build_no.decimalized_SHA
# environment is generated from Scheme
# RC,build_no are retrieved from VersioningPropertie.plist
# decimalized_SHA is generated using another script
#

BASH_SOURCE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# generate decimalized SHA
DECIMALIZED_GIT_HASH="$( cd "${BASH_SOURCE_DIR}"; ./decimalize_git_hash.bash $( git rev-parse --short=7 HEAD ) )"
echo "Decimalized: \"${DECIMALIZED_GIT_HASH}\""

# generate environment
if [[ ${CONFIGURATION} == "Release_LIVE" ]]; then
ENVIROMENT_NUMBER=9
elif [[ ${CONFIGURATION} == "Release_QA" ]]; then
ENVIROMENT_NUMBER=1
elif [[ ${CONFIGURATION} == "Release_FIT" ]]; then
ENVIROMENT_NUMBER=2
elif [[ ${CONFIGURATION} == "Release_SIT" ]]; then
ENVIROMENT_NUMBER=3
elif [[ ${CONFIGURATION} == "Release_MIG" ]]; then
ENVIROMENT_NUMBER=4
elif [[ ${CONFIGURATION} == "Release_UAT" ]]; then
ENVIROMENT_NUMBER=5
elif [[ ${CONFIGURATION} == "Release_PRELIVE" ]]; then
ENVIROMENT_NUMBER=6
elif [[ ${CONFIGURATION} == "Release" ]]; then
ENVIROMENT_NUMBER=0
else
ENVIROMENT_NUMBER=1
fi

AFSE_BUNDLE_VERSION="${ENVIROMENT_NUMBER}"."${RC}"."${BUILD}"."${DECIMALIZED_GIT_HASH}"

plist="$TARGET_BUILD_DIR/$INFOPLIST_PATH"

/usr/libexec/PlistBuddy -c "Set :CFBundleShortVersionString ${AFSE_BUNDLE_VERSION}" "${plist}" 
 